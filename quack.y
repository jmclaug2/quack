%{
void yyerror(char *s); /*C decs*/
#include <stdio.h>
#include <stdlib.h>
extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
 
void yyerror(const char *s);
%}
%union {
	int ival;
	char cval;
	char *sval;
}

%left '<' '>' '==' ATLEAST ATMOST
%left '+' '-'
%left '*' '/'

%%

program			: 
				| classes statements
				;

classes			: 
				| classes class
				; 

class			: class_sig class_body;

class_sig 		: CLASS IDENT '(' formal_args ')' EXTENDS IDENT 
				| CLASS IDENT '(' formal_args ')' 
				;

formal_args		:
				| formal_args formal_arg
				;

formal_arg 		| IDENT ':' IDENT
				;

class_body 		: '{' '}'
				| '{' statements methods '}'
				;

methods 		:
				| methods method

method 			: DEF IDENT '(' formal_args ')' ':' IDENT statement_block
				| DEF IDENT '(' formal_args ')' statement_block
				;

statement_block	: '{' '}'
				| '{' statements '}' 
				;

statements		:
				| statements statement
				;

statement 		: IF r_expr statement_block u_elif u_else
				| IF r_expr statement_block "elif" statement_block
				| WHILE r_expr statement_block
				| RETURN ';'
				| RETURN r_expr ';'
				| l_expr ':' IDENT '=' r_expr ';'
				| l_expr '=' r_expr ';'
				| r_expr ';'
				;

u_elifs 		:
				| u_elifs u_elif 
				;

u_elif 			: ELIF r_expr statement_block
				;

u_else 			: 
				| ELSE statement_block
				;

l_expr			: IDENT
				| r_expr '.' IDENT
				;

r_expr 			: string_literal
				| integer_literal
				| l_expr
				| r_expr '+' r_expr
				| r_expr '-' r_expr
				| r_expr '*' r_expr
				| r_expr '/' r_expr
				| '(' r_expr ')'
				| r_expr EQUALS r_expr
				| r_expr ATMOST r_expr
				| r_expr '<' r_expr
				| r_expr ATLEAST r_expr
				| r_expr '>' r_expr
				| r_expr AND r_expr
				| r_expr OR r_expr
				| r_expr NOT r_expr
				| r_expr '.' IDENT '(' actual_args ')'
				| IDENT '(' actual_args ')'
				;

actual_args 	: 
				| actual_arg r_expr
				;

actual_arg 	 	: 
				| actual_arg r_expr
				;


%%	
