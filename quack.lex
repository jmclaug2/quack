%{
		#include <stdio.h>

        int num_lines = 1, num_chars = 0;

        int x = 200;
%}



%x BLOCK_COMMENT
%%
\n      ++num_lines; ++num_chars;
[ \t\r\f] 			/* do nothing */

"/*"            	{BEGIN(BLOCK_COMMENT);}
<BLOCK_COMMENT>"*/" {BEGIN(INITIAL);}
<BLOCK_COMMENT>\n   {}
<BLOCK_COMMENT>.    {}


"\"\"\""([^']|'[^']|''[^'])*"\"\"\""	{return 1;}

[/]{2}+.*			{}
\"(\\[0bftnr\\]|[^"\n\\])*\" {return 2;}
\"(\\.|[^"]|\n)*\"	{return 21;}


class		{return 3;}
def 		{return 4;}
extends		{return 5;}
if			{return 6;}
elif		{return 7;}
else		{return 8;}
while		{return 9;}
return		{return 10;}

String		{return 11;}	
Integer		{return 11;}
Obj			{return 11;}
Boolean		{return 11;}
true		{return 11;}
false		{return 11;}
and			{return 17;}
or			{return 18;}
not			{return 19;}
Nothing		{return 11;}
none		{return 11;}

[_a-zA-Z][_a-zA-Z0-9]* 	{return 11;}
[1-9][0-9]*	{return 12;}
0			{return 12;}

"+"			{return '+';}
"-"			{return '-';}
"*"			{return '*';}
"/"			{return '/';}

"="{2}		{return 13;}
"<="		{return 14;}
"<"			{return '<';}
">="		{return 16;}
">"			{return '>';}

"{"			{return '{';}
"}"			{return '}';}
"("			{return '(';}
")"			{return ')';}
","			{return ',';}
"."			{return '.';}
";"			{return ';';}
":"			{return ':';}

"="			{return '=';}

<<EOF>>		{return 0;}

.			{return 20;}


%%



