#ifndef __TOKNAMES_H__
#define __TOKNAMES_H__

struct tok_name_pair { int num;  char* name; };

typedef struct tok_name_pair tok_name;

static tok_name token_name_table[] = {

  { 1, "STRING_LIT" }
 ,{ 2, "STRING_LIT" }
 ,{ 3, "CLASS" 		}
 ,{ 4, "DEF" 		}
 ,{ 5, "EXTENDS" 	}
 ,{ 6, "IF" 		}
 ,{ 7, "ELIF" 		}
 ,{ 8, "ELSE" 		}
 ,{ 9, "WHILE" 		}
 ,{ 10, "RETURN" 	}
 ,{ 11, "IDENT" 	}
 ,{ 12, "INT_LIT" 	}
 ,{ 16, "ATLEAST" 	}
 ,{ 14, "ATMOST" 	}	
 ,{ 13, "EQUALS" 	}
 ,{ 17, "AND" 		}
 ,{ 18, "OR" 		}
 ,{ 19, "NOT" 		}


};
int num_named_tokens = 19;

#endif

