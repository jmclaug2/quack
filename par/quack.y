%{
#include <cstdio>
#include <iostream>
#include <unistd.h> 
#include "AST.h"
using namespace std;

// stuff from flex that bison needs to know about:
extern "C" int yylex();
extern "C" int yyparse();
extern "C" FILE *yyin;
extern "C" int yylineno;

 
void yyerror(const char *s);
%}

%locations

%union {
	int ival;
	char cval;
	char *sval;
}

%token <sval> CLASS
%token <sval> DEF
%token <sval> EXTENDS
%token <sval> IF
%token <sval> ELIF
%token <sval> ELSE
%token <sval> WHILE
%token <sval> RETURN
%token <sval> ATLEAST
%token <sval> ATMOST
%token <sval> EQUALS
%token <sval> AND
%token <sval> OR
%token <sval> NOT
%token <sval> IDENT
%token <ival> INT_LIT
%token <sval> STRING_LIT
%token <cval> '+'
%token <cval> '-'
%token <cval> '*'
%token <cval> '/'
%token <cval> '<'
%token <cval> '>'
%token <cval> '{'
%token <cval> '}'
%token <cval> '('
%token <cval> ')'
%token <cval> ','
%token <cval> '.'
%token <cval> ';'
%token <cval> ':'

%left '.'
%left AND OR NOT
%left '<' '>' ATLEAST ATMOST EQUALS 
%left '+' '-'
%left '*' '/'


%%

program			: classes statements 		{}
				;

classes			: 							{}
				| classes class 			{}
				; 

class			: class_sig class_body 		{}
				;

class_sig 		: CLASS IDENT '(' u_formal_args ')' extends {}
				;

u_formal_args	: 					{}
				| formal_args 		{}
				;

formal_args		: formal_arg						{}
				| formal_args ',' formal_arg 	{}
				;

formal_arg 		: IDENT ':' IDENT	{}
				;

extends 		:					{}
				| EXTENDS IDENT 	{}
				;

class_body 		: '{' statements methods '}' 	{}
				;

methods 		: 					{}
				| methods method 	{}
				;

method 			: DEF IDENT '(' u_formal_args ')' type_id statement_block 	{}
				;

statement_block	: '{' statements '}' 			{}
				;

statements		:								{}
				| statements statement 			{}
				;

statement 		: IF r_expr statement_block elifs else 	{}
				| WHILE r_expr statement_block 			{}
				| RETURN ';' 							{}
				| RETURN r_expr ';' 					{}
				| l_expr type_id '=' r_expr ';' 		{}
				| r_expr ';' 							{}
				;

type_id 		: 			{}
				|':' IDENT 	{}
				;

elifs 			: 				{}
				| elifs elif 	{}
				;

elif 			: ELIF r_expr statement_block 	{}
				;

else 			: 							{}
				| ELSE statement_block		{}
				;

l_expr			: IDENT				{}
				| r_expr '.' IDENT	{}
				;

r_expr 			
				: l_expr				{}
				| '(' r_expr ')'		{}
				| r_expr '*' r_expr 	{}
				| r_expr '/' r_expr 	{}
				| r_expr '+' r_expr		{}
				| r_expr '-' r_expr		{}
				| r_expr EQUALS r_expr	{}
				| r_expr ATMOST r_expr	{}
				| r_expr '<' r_expr		{}
				| r_expr ATLEAST r_expr	{}
				| r_expr '>' r_expr		{}
				| r_expr AND r_expr		{}
				| r_expr OR r_expr		{}
				| r_expr NOT r_expr		{}
				| r_expr '.' IDENT '(' u_actual_args ')'	{}
				| IDENT '(' u_actual_args ')'				{}
				| STRING_LIT 			{}
				| INT_LIT 				{}
				;

u_actual_args	:
				| actual_args {}
				;

actual_args 	: actual_arg 					{}
				| actual_args ',' actual_arg 	{}
				;

actual_arg 	 	: r_expr 	{}
				;


%%	

char *filename = (char *) malloc(sizeof(char)*255); 


int main(int argc, char** argv) {

	FILE *f;
	f = stdin;
	if (argc > 1) {
    	if( !(f = fopen(argv[1], "r"))) {
			perror(argv[1]);
			exit(1);
		}
	}

	if (!f) {
		cout << "File could not be opened!" << endl;
		return -1;
	}
	// set flex to read from it instead of defaulting to STDIN:
	yyin = f;
	int rv;

	int fd; 
  	char fd_path[255]; 

  	fd = fileno(f); 
  	sprintf(fd_path, "/proc/self/fd/%d", fd); 
  	readlink(fd_path, filename, 255); 

	do {
		rv = yyparse();
	} while (!feof(yyin));

	cout << "Finished parse with no errors" << endl;
	free(filename);

}

void yyerror(const char *s) {


	cout << filename << ":" << yylineno << ": " << s <<" (at \'"<< yylval.sval <<"\')"<< endl;
	free(filename);
	exit(-1);
}