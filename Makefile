

all: finddupl

clean:
	rm -f *.o scanner*

finddupl: scan.o lex.yy.o
	cc -o scanner scan.o lex.yy.o -lfl

scan.o: scan.c lex.yy.h 
lex.yy.o: lex.yy.c

