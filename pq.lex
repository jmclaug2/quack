%{
		#include <stdio.h>
		#include "quack.tab.h"
%}



%x BLOCK_COMMENT
%%
\n      ++num_lines; ++num_chars;
[ \t\r\f] 			/* do nothing */

"/*"            	{BEGIN(BLOCK_COMMENT);}
<BLOCK_COMMENT>"*/" {BEGIN(INITIAL);}
<BLOCK_COMMENT>\n   {}
<BLOCK_COMMENT>.    {}


"\"\"\""([^']|'[^']|''[^'])*"\"\"\""	{return STRING_LIT;}

[/]{2}+.*			{}
\"(\\[0bftnr\\]|[^"\n\\])*\" {return STRING_LIT;}
\"(\\.|[^"]|\n)*\"	{return 21;}


class		{return CLASS;}
def 		{return DEF;}
extends		{return EXTENDS;}
if			{return IF;}
elif		{return ELIF;}
else		{return ELSE;}
while		{return WHILE;}
return		{return RETURN;}

String		{return IDENT;}	
Integer		{return IDENT;}
Obj			{return IDENT;}
Boolean		{return IDENT;}
true		{return IDENT;}
false		{return IDENT;}
and			{return AND;}
or			{return OR;}
not			{return NOT;}
Nothing		{return IDENT;}
none		{return IDENT;}

[_a-zA-Z][_a-zA-Z0-9]* 	{return IDENT;}
[1-9][0-9]*	{return INT_LIT;}
0			{return INT_LIT;}

"+"			{return '+';}
"-"			{return '-';}
"*"			{return '*';}
"/"			{return '/';}

"="{2}		{return EQUALS;}
"<="		{return ATMOST;}
"<"			{return '<';}
">="		{return ATLEAST;}
">"			{return '>';}

"{"			{return '{';}
"}"			{return '}';}
"("			{return '(';}
")"			{return ')';}
","			{return ',';}
"."			{return '.';}
";"			{return ';';}
":"			{return ':';}

"="			{return '=';}

<<EOF>>		{return 0;}

.			{return 20;}


%%



