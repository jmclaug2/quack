#include <stdio.h>
#include <stdlib.h>
#include "builtin/Builtins.h"
#include "gc.h"
void quackmain(); 
int main(int argc, char** argv) {
  quackmain();
  printf("\n--- Terminated successfully ---\n");
  exit(0);
}
struct class_A_struct;
typedef struct class_A_struct* class_A;
struct class_A_struct the_class_A_struct;
struct obj_A_struct;
typedef struct obj_A_struct* obj_A;
struct obj_A_struct {
  class_A clazz;
obj_Int value_;
};
struct class_A_struct {
obj_A (*constructor) (obj_Int);
obj_Nothing (*MY_PRINT) (obj_A);
obj_A (*a) (obj_A);
obj_Obj (*PRINT) (obj_Obj);
obj_String (*STRING) (obj_Obj);
obj_Boolean (*EQUALS) (obj_Obj, obj_Obj);
};
extern class_A the_class_A;
obj_A new_A(obj_Int value_) {
obj_A this_ = (obj_A) GC_MALLOC(sizeof(struct obj_A_struct));
this_->clazz = the_class_A;
void *rv_;
void *true_ = lit_true;
void *false_ = lit_false;
this_->value_=value_;
return this_;
}
obj_Nothing A_method_MY_PRINT(obj_A this_) {
void *false_ = lit_false;
void *true_ = lit_false;
void *return_;
((obj_Int)this_->value_)->clazz->PRINT(((obj_Obj)this_->value_));
}
obj_A A_method_a(obj_A this_) {
void *false_ = lit_false;
void *true_ = lit_false;
void *rv_;
void *return_;
if ((((obj_Int)int_literal(0))->clazz->LESS(((obj_Int)int_literal(0)), ((obj_Int)this_->value_)))==lit_true)
{rv_=the_class_A->constructor(((obj_Int)((obj_Int)this_->value_)->clazz->SUB(((obj_Int)this_->value_), ((obj_Int)int_literal(1)))));
((obj_A)rv_)->clazz->MY_PRINT(((obj_A)rv_));
((obj_String)str_literal(", "))->clazz->PRINT(((obj_String)str_literal(", ")));
return ((obj_A)rv_)->clazz->a(((obj_A)rv_));
}
else {return the_class_A->constructor(((obj_Int)this_->value_));
}
}
struct class_A_struct the_class_A_struct ={
 new_A, A_method_MY_PRINT
, A_method_a
, Obj_method_PRINT
, Obj_method_STRING
, Obj_method_EQUALS
};
class_A the_class_A= &the_class_A_struct;
void quackmain() {
/*
STMT_ASSIGN
STATEMENT
STATEMENT
STMT_ASSIGN
STATEMENT
STATEMENT
*/
void *true_=lit_true;
void *false_=lit_false;
void *a_;
a_=the_class_A->constructor(((obj_Int)int_literal(5)));
((obj_A)a_)->clazz->MY_PRINT(((obj_A)a_));
((obj_String)str_literal("\n"))->clazz->PRINT(((obj_String)str_literal("\n")));
a_=((obj_A)a_)->clazz->a(((obj_A)a_));
((obj_String)str_literal("\n"))->clazz->PRINT(((obj_String)str_literal("\n")));
((obj_A)a_)->clazz->MY_PRINT(((obj_A)a_));
}